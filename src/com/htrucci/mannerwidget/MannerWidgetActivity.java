package com.htrucci.mannerwidget;

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

import android.app.PendingIntent;
import android.app.Service;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.os.IBinder;
import android.util.Log;
import android.view.SurfaceHolder;
import android.widget.RemoteViews;




public class MannerWidgetActivity extends AppWidgetProvider implements SurfaceHolder.Callback {

    private static final String ACTION_MANNER_ON ="com.htrucci.mannerwidget.ON";
	private static final String ACTION_MANNER_OFF ="com.htrucci.mannerwidget.OFF";
	private ComponentName MannerWidget;
	private RemoteViews views = null;
	int mannerControl = 0;
	private Intent intent;
	private AudioManager audioManager = null;	
	private int whichStream = AudioManager.STREAM_RING;
	
	
	@Override
	public void onEnabled(Context context) {
		// TODO Auto-generated method stub
		AppWidgetManager manager = AppWidgetManager.getInstance(context);
		Timer timer = new Timer();
		timer.scheduleAtFixedRate(new MyTime(context, manager), 1, 10000);
		super.onEnabled(context);
	}

	@Override
	public void onDeleted(Context context, int[] appWidgetIds) {
		// TODO Auto-generated method stub
		super.onDeleted(context, appWidgetIds);
	}

	@Override
	public void onUpdate(Context context, AppWidgetManager appWidgetManager,
			int[] appWidgetIds) {

		// TODO Auto-generated method stub
		Log.e("Widget State","onUpdate");
		audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		int curring = audioManager.getRingerMode();
		int curvol = audioManager.getStreamVolume(whichStream);
		MannerWidget = new ComponentName(context, MannerWidgetActivity.class);
		views = new RemoteViews(context.getPackageName(), R.layout.main);
		Log.e("Widget State","RingerMode == NULL");
		if(audioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE)
		{
			Log.e("Widget State","RingerMode == VIBRATE");
			mannerControl = 1;
			views.setImageViewResource(R.id.flash_btn, R.drawable.on);
			intent = new Intent(ACTION_MANNER_OFF);
		}else if(audioManager.getRingerMode() == AudioManager.RINGER_MODE_NORMAL)
		{
			Log.e("Widget State","RingerMode == NORMAL");
			mannerControl = 2;
			views.setImageViewResource(R.id.flash_btn, R.drawable.off);
			intent = new Intent(ACTION_MANNER_ON);
		}
		// Flash Intent
		PendingIntent onPendingIntent = PendingIntent.getBroadcast(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		views.setOnClickPendingIntent(R.id.flash_btn, onPendingIntent);
		
		appWidgetManager.updateAppWidget(MannerWidget, views);
	}
	
	@Override
	public void onReceive(Context context, Intent intent) {
		// TODO Auto-generated method stub
		String action = intent.getAction();
		Log.e("Widget State","onReceive");
		Log.e("Flash state", intent.getAction());
       
        Log.e("Widget State","onReceive3");
		
		
	    if(action.equals(ACTION_MANNER_ON)){
	    	
			Log.e("Flash state", intent.getAction());
			
			 audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			 if(audioManager.getRingerMode() == AudioManager.RINGER_MODE_NORMAL){
			 audioManager.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
			 audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 0, AudioManager.FLAG_PLAY_SOUND);
			 }

			try{
				mannerControl = 1;
				
				AppWidgetManager manager = AppWidgetManager.getInstance(context);
			    this.onUpdate(context, manager, manager.getAppWidgetIds(new ComponentName(context, MannerWidgetActivity.class)));
			    
			}catch (Exception e) {
				// TODO: handle exception
				Log.e("Flash state", "Flash ON Exception");
			}
		}else if(action.equals(ACTION_MANNER_OFF)){
			 
			 Log.e("Flash state", "ACTION MANNER OFF");
			 audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
			 if(audioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE){
			 audioManager.setRingerMode(AudioManager.RINGER_MODE_NORMAL);
			 audioManager.setStreamVolume(AudioManager.STREAM_MUSIC, 8, AudioManager.FLAG_PLAY_SOUND);
			 }
			 Log.e("Flash state", "ACTION MANNER OFF");
			 
			try{

				mannerControl = 0;
				
				AppWidgetManager manager = AppWidgetManager.getInstance(context);
			    this.onUpdate(context, manager, manager.getAppWidgetIds(new ComponentName(context, MannerWidgetActivity.class)));
			
			}catch (Exception e) {
				// TODO: handle exception
				Log.e("Flash state", "Flash OFF Exception");
			}
		}else{
			super.onReceive(context, intent);
		}
	}

	@Override
	public void surfaceChanged(SurfaceHolder holder, int format, int width,
			int height) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceCreated(SurfaceHolder holder) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void surfaceDestroyed(SurfaceHolder holder) {
		// TODO Auto-generated method stub
	
	}

	class MyTime extends TimerTask{
		RemoteViews remoteviews;
		AppWidgetManager appwidgetmanager;
		ComponentName componentname;
		
		MyTime(Context context, AppWidgetManager appWidgetManager){
			appwidgetmanager = appWidgetManager;
			remoteviews = new RemoteViews(context.getPackageName(), R.layout.main);
			componentname = new ComponentName(context, MannerWidgetActivity.class);
		}
		@Override
		public void run() {
			Log.e("run", "run!");
			 if(audioManager.getRingerMode() == AudioManager.RINGER_MODE_NORMAL){
				 Log.e("run", "Normal");
				 remoteviews.setImageViewResource(R.id.flash_btn, R.drawable.off);
				 
			 }else{
				 Log.e("run", "Vibrate");
				 remoteviews.setImageViewResource(R.id.flash_btn, R.drawable.on);
			 }
			 appwidgetmanager.updateAppWidget(componentname, remoteviews);
			
		}
		
	}
		
		
	public static class UpdateService extends Service{

		public IBinder onBind(Intent intent) {
			return null;
		}

		@Override
		public int onStartCommand(Intent intent, int flags, int startId) {
			Log.d("TAG","onStartCommand");
			RemoteViews remoteViews = buildUpdate(this);
			Log.d("TAG", "update �������");
			
			ComponentName thisWidget = new ComponentName(this, MannerWidgetActivity.class);
			AppWidgetManager manager = AppWidgetManager.getInstance(this);
			manager.updateAppWidget(thisWidget, remoteViews);
			Log.d("TAG", "���� ������Ʈ��");
			return super.onStartCommand(intent, flags, startId);
		}
		private RemoteViews buildUpdate(Context context){
			RemoteViews views = new RemoteViews(context.getPackageName(),R.xml.widgetprovider);
			return views;
		}
		
	}


}
